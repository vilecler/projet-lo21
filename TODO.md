# Liste des choses à faire dès que vous avez le temps

## TOUT LE MONDE
- **Installer Qt (c'est très long)**

### Vivien et Boris
- Commencer la maquette de l'application et des différentes vues
- Implémenter la maquette sans le code et tester l'affichage + les widgets
- Ajouter les images de la charte graphique
- Implémenter toute l'interface graphique
- Tester la charte graphique

### Martin
- Développer le code des class CompteManager et CompteRacine 

### Thomas et Victor
- Implémenter les différents types de comptes (CompteActifs, ComptePassifs, CompteRecettes et CompteDepenses) à partir de la classe Compte
- Implémenter la classe Triplet
- Tester ces classes (en particulier tester les méthodes virtuelles et le polymortphisme)

### Bastien
- Mise en place du TransactionManager et de la classe Transaction

# Ce qu'il reste à faire
- Création d'une classe Date commune à tout le monde.
- Gestion des exceptions
- Tester l'application
- Fonctions de création de l'association et fonction de cloture du compte
- Fonction de bilan d'activité
- Tester l'application
- Création d'un jeu de données
- Lecture et sauvegarde dans un fichier (déterminer si XML ou JSON)
- Lier les différents modules entre eux
- Ecriture d'un rapport