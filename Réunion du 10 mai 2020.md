## Compte rendu de la réunion du 10 mai 2020

Ajouter vos codes sur le GitLab svp

### Travail réalisé jusqu'à présent
- Compte racine et compte manager refait (Boris) fait par Martin et affichage dans une fenêtre. Besoin d'implémenter les comptes pour avancer. Besoin de l'architecture.
- Fonction charger et enregistrer déjà bien avancée

### Travail à réaliser
- Passer QML
- Vivien : Afficher les transactions dans un tableau
- Enregistrer
- Ouvrir
- Action créer compte : par type de compte (indiquer pere)
- Ajouter transaction : entre 2 comptes
- Cloturer comptes 
- Compte.getPere() : Compte (ajouter UML)
- Ajouter un fichier de config pour restaurer le contexte (peut etre pas besoin de se prendre la tete avec du QML)
- Ajouter boolean transactions rapprochée ou non
- Possibilité de modifier une transaction GUI (si pas rapproché)
- Ajouter fonction de recherche de transactions dans l'UML
- Ajouter nombre de triplets d'une transaction dans l'UML