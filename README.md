# Projet LO21

### Membres du groupe
- Vivien Leclercq
- Martin Boyer
- Thomas Laurent
- Victor Kerjean
- Boris Legal
- Bastien Hubacher

Ce projet correspond à l'UV LO21 de l'UTC enseignée en P20 par Antoine Jouglet.  
Nous avons décidé d'appeler notre projet TurboTréso

### Repartition des taches
- Vivien : Graphisme
- Boris : Graphisme
- Martin :  Compte amanager + racine
- Thomas Victor : Tous les types de comptes + triplets
- Bastien Trasnsactions manager + transaction